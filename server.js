var express = require('express'), // import
  app = express(), // inicialización
  port = process.env.PORT || 3000; // puerto predefenido o asigna el 3000

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);



//API de Express, funciones GET y POST. ("ruta", function)
app.get('/', function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/', function(req, res){
  res.send('Hemos recibido su petición cambiada');

})


//app.get('/clientes/:idcliente', function(req, res){
//  res.send('Aquí tiene al cliente número ' + req.params.idcliente));
//})

app.get('/clientes/:idcliente', function(req, res){
  res.sendFile(path.join(__dirname, req.params.idcliente + '.json'));
})

app.get('/clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'get.json'));
})

app.post('/clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'post.json'));
})

app.put('/clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'put.json'));
})

app.delete('/clientes', function(req, res){
  res.sendFile(path.join(__dirname, 'delete.json'));
})
